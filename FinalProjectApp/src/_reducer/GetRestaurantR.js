
const initialState = {
    dataRestaurant: [],
    isLoading: false,
    error: false,
    user: "",
  };
  
 const GetRestaurantR = (state = initialState, action) => {
    switch (action.type) {
      case "GET_RESTAURANT_PENDING":
        return {
          ...state,
          isLoading: true,
        };
      case "GET_RESTAURANT_FULFILLED":
        return {
          ...state,
          dataRestaurant: action.payload.data.restaurants.restaurant,
          user: action.payload.data,
          isLoading: false,
          isLoaded: true
        };
      case "GET_RESTAURANT_REJECTED":
        return {
          ...state,
          isLoading: false,
          error: true,
        };
      default:
        return state;
    }
  };

  export default GetRestaurantR