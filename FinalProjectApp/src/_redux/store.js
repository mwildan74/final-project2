import { createStore, combineReducers, applyMiddleware } from "redux";
import logger from "redux-logger";
import promise from "redux-promise-middleware"
import LoginR from '../_reducer/LoginR'
import CheckUserR from '../_reducer/CheckUserR'
import GetRestaurantR from '../_reducer/GetRestaurantR'


const reducers = combineReducers({
 LoginR,
 CheckUserR
});

const store = createStore(reducers, applyMiddleware(promise, logger));

export default store;