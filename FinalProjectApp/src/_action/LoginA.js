import axios from "axios";

export const LoginA = data => {
  return {
    type: "LOGIN",
    payload: axios({
      method: "POST",
      url: "https://penggadaian.herokuapp.com/api/v1/login",
      data
    })
  };
};
