import axios from "axios";
import {AsyncStorage} from 'react-native'

const token = AsyncStorage.getItem("token")

export const CheckUserA = data => {
  return {
    type: "CHECK_USER",
    payload: axios({
      method: "GET",
      url: "https://penggadaian.herokuapp.com/api/v1/cekuser",
      headers: {
      Authorization: `Bearer ${token}`
      },
      data
    })
  };
};
