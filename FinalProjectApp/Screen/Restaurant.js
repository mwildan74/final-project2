import React, {useState,useEffect} from 'react';
import {View, Image} from 'react-native';
import axios from 'axios';
import {Content, Card, CardItem, Left, Right, Icon, Text} from 'native-base';


const Restaurant = ({route, navigation}) => {


    const { nama_restaurant,res_id,link } = route.params;
    const [restaurant, setRestaurant] = useState([]);
    const [load, setLoad] = useState(false);
    const [error, setError] = useState('');
 

    useEffect(() => {
        axios.get(`https://developers.zomato.com/api/v2.1/restaurant?res_id=${res_id}`,{
        headers:{
            "user-key":"5f9fc976b65f5a2256e54bfdeb7e15b5"
        }   
        })
            .then(res => {
                setRestaurant(res.data);
                setLoad(true);
            })
            .catch(err => {
                setError(err.message);
                setLoad(true)
            })
    }, []);

    

    console.log(restaurant)

    let alamat = {...restaurant.location};
    let rating = {...restaurant.user_rating};
    return (
      <View style={{flex: 1}}>
        <Content>
          <Card>
            <CardItem cardBody>
              <Image
                style={{height: 240, width: null, flex: 1}}
                source={{uri: restaurant.featured_image}}
              />
            </CardItem>
            <CardItem>
              <Left>
                <Text>Alamat Restaurant : {alamat.address}</Text>
              </Left>
            </CardItem>
            <CardItem>
              <Left>
                <Icon name="star" style={{backgroundColor: '#e1ee17'}} />
                <Text>{rating.aggregate_rating}</Text>
              </Left>
              <Right>
                <Icon name="chatbubbles" />
                <Text>{restaurant.all_reviews_count}</Text>
              </Right>
            </CardItem>
          </Card>

          <Card>
            <CardItem>
              <Left>
                <Text>
                  Jenis Masakan : {restaurant.cuisines}
                </Text>
              </Left>
            </CardItem>
          </Card>
        </Content>
      </View>
    );
  
}

export default Restaurant;