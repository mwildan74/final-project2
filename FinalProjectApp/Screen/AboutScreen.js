import React, { Component } from 'react'
import { StyleSheet, Text, Image, View, TouchableOpacity, Linking } from 'react-native'

import { robotoWeights, material, human } from 'react-native-typography'


export default class About extends React.Component {
    render() {
        return (
            <>
                <View style={{ backgroundColor: 'white', }}>
                    <View style={{ backgroundColor: 'white', justifyContent: 'center', alignItems: "center" }}>
                        <Image source={require('./assets/aboutprofilew.jpg')} style={styles.profile} />
                        <Text style={human.headline} style={{ marginTop: 10, fontSize: 20 }}>Muhammad Wildan</Text>
                    </View>
                    <View style={{ alignItems: "center", justifyContent: "center" }}>
                        <View style={{
                            backgroundColor: 'white', height: 100, width: "90%", borderWidth: 1, marginTop: 20, borderColor: '#fd0759', shadowColor: "#000",
                            shadowOffset: {
                                width: 0,
                                height: 7,
                            },
                            shadowOpacity: 0.41,
                            shadowRadius: 9.11,

                            elevation: 14, borderRadius: 12
                        }}>
                            <Text style={robotoWeights.bold} style={{ marginLeft: 5, marginTop: 5 }}>Hi, My Name is Wildan, I'm an Front End Development that currenctly focus on developing Front end using React Native
                            For Mobile Development, and now I'm searching for a job</Text>
                        </View>
                    </View>
                    <Text style={{ marginTop: 50, fontWeight: 'bold', marginLeft: 10 }}>Connect with me</Text>



                    <View style={{ flexDirection: 'row', justifyContent: "space-around", marginTop: 5, backgroundColor: 'white' }}>
                        <TouchableOpacity style={{ width: 100, height: 100 }} onPress={() =>
                            Linking.openURL(
                                'shorturl.at/flUV4',
                            )
                        }>
                            <Image source={require('./assets/Linkedinicon.png')} />
                        </TouchableOpacity>

                        <TouchableOpacity style={{ width: 100, height: 100 }} onPress={() =>
                            Linking.openURL(
                                'https://www.instagram.com/justwil12/'
                            )
                        }>
                            <Image source={require('./assets/instagram.png')} />
                        </TouchableOpacity>

                        <TouchableOpacity style={{ width: 100, height: 100 }} onPress={() => Linking.openURL('gitlab.com/mwildan74')}>
                            <Image source={require('./assets/githubicon.png')} />
                        </TouchableOpacity>

                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                        <TouchableOpacity style={{ width: 100, height: 100 }} onPress={() => Linking.openURL('http://facebook.com/wildan.gamer')}>
                            <Image source={require('./assets/facebookicon.png')} />
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: 100, height: 100 }} onPress={() => Linking.openURL('https://api.whatsapp.com/send?phone=+6283820810052&text=Halo%20gan%20mau%20tanya?')}>
                            <Image source={require('./assets/waicon.png')} />
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: 100, height: 100 }}>
                            <Image source={require('./assets/twitter.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ height: 200, backgroundColor: 'white' }}>

                    </View>
                </View>
            </>
        )
    }
}


const styles = StyleSheet.create({
    profile: {
        width: 150,
        height: 150,
        borderRadius: 150 / 2,
        overflow: "hidden",
        borderColor: "black",
        padding: 30
    }


})